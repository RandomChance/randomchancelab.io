---
title: Code Sample
subtitle: Using Hugo or Pygments
date: 2016-03-08
tags: ["example", "code"]
---

The following are two code samples using syntax highlighting.

<!--more-->

The following is a code sample using triple backticks ( ``` ) code fencing provided in Hugo. This is client side highlighting and does not require any special installation.

```javascript
    var num1, num2, sum
    num1 = prompt("Enter first number")
    num2 = prompt("Enter second number")
    sum = parseInt(num1) + parseInt(num2) // "+" means "add"
    alert("Sum = " + sum)  // "+" means combine into a string
```


```powershell
    $Temp = Get-Date
    '{0:d} {0:HH:mm:ss:ttt}' -f $temp
```
